"""
表单数据验证
"""
import re

from django import forms
from django.core.exceptions import ValidationError

from .BaseValidate import BaseValidate


# 自定义验证方法
def mobile_validate(value):
    # 使用正则对手机号进行验证
    mobile_re = re.compile(r'^(13[0-9]|15[0123456789]|17[678]|18[0-9]|14[57])[0-9]{8}$')
    if not mobile_re.match(value):
        # 不满足返回 手机号码格式错误
        raise ValidationError('手机号码格式错误')


class TestValidate(forms.Form, BaseValidate):
    """
    此处是对用户名称验证，判断是否为空，为空会输出提示信息
    """
    user = forms.CharField(required=True, error_messages={'required': '用户名不能为空'})


    """
    邮箱验证  不能为空
    """
    email = forms.EmailField(required=True, error_messages={'required': '邮箱不能为空'})

    """
    电话号码验证  不能为空，不为空调用mobile_validate
    """
    phone = forms.CharField(
        required=True,
        error_messages={
            'required': '电话号码不能为空'
        }, validators=[mobile_validate, ]  # 验证方法加载
    )

    """
    密码验证为空会提示   密码不能为空,<6位会提示,大于10位会提示
    """
    pwd = forms.CharField(
        required=True,
        min_length=6,
        max_length=10,
        error_messages={
            'required': '密码不能为空',
            'min_length': '密码长度不能小于6',
            'max_length': '密码长度不能大于10'
        }
    )

    """
    年龄验证  不能为空，只能是整形数字，不为数字
    """
    num = forms.IntegerField(  # IntegerField整型
        required=True,
        max_value=150,  # 最大值
        min_value=0,  # 最小值
        error_messages={
            'required': '年龄不能为空',
            'max_value': '年龄不能大于150',
            'min_value': '年龄不能小于0',
            'invalid': '必须输入数字'
        }

    )

    """
    测试
    """
    test_choices = (
        (0, '北京'),
        (1, '重庆'),
        (2, '成都'),
        (3, '上海')
    )
    test = forms.IntegerField(
        required=True,
        widget=forms.Select(choices=test_choices),
        error_messages = {
            'required': '测试不能为空',
        }
    )

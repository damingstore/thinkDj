from django.urls import path
from .controller import TestController

urlpatterns = [
    path('test1/', TestController.test1),
    path('upload/', TestController.upload),
    path('captcha/', TestController.captcha),
    path('qrcode/', TestController.qrcode),
    path('req/', TestController.req),
]
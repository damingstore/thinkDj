import os

from django.conf import settings
from django.http import HttpResponse, JsonResponse
from django.views.decorators.http import require_http_methods, require_GET, require_POST
from app.system.dao.TestDao import TestDao
from app.system.validate.TestValidate import TestValidate
from utils.services.JwtService import JwtServices
from utils.services.ResponseService import ResponseService
from utils.services.UploadService import UploadService
from utils.services.CaptchaService import CaptchaService
from utils.services.QrcodeService import QrcodeService
from utils.services.RequestService import RequestService


@require_GET
def test1(request):
    # print(request)
    # testDao = TestDao()
    # return HttpResponse('system_'+ obj['text'])
    # return HttpResponse(testDao.test('arg'))

    # 参数验证
    # validate = TestValidate(request.GET).validate()
    # if not validate:
    #     return HttpResponse('err')
    # return HttpResponse('test2')

    # 生成token
    dit = {'name': 1111, 'age': 11}
    ret = JwtServices().createToken(dit)
    ResponseService.sucess().data(ret)
    #
    # # 解析token
    # ret = JwtServices().parseToken(request)
    # if ret:
    #     userInfo = request.userInfo
    #     return JsonResponse(ResponseService.sucess().data(userInfo))

    # 刷新token
    dit = {'name': 1111, 'age': 11}
    ret = JwtServices().createRefreshToken(dit)
    ResponseService.sucess().data(ret)

    # return HttpResponse(testDao.testDao())
    # return HttpResponse('test1')


"""
测试文件上传
"""


@require_POST
def upload(request):
    file = request.FILES.get('file')
    ret = UploadService().store(file, 'me/me/aa')
    ResponseService.sucess().data(ret)


"""
测试文件上传
"""


@require_GET
def captcha(request):
    # 生成验证码
    # ret = CaptchaService().captcha('5555')
    # r=dict()
    # r['t'] = ret['text']
    # return ResponseService.sucess().data(r)
    # 返回二进制图片时需要指定content_type
    # return HttpResponse(ret["image"], content_type="image/png")

    # 验证验证码
    ret = CaptchaService().checkCaptcha('3FC9', '5555')
    if ret:
        return ResponseService.sucess().data()
    else:
        return ResponseService.error('验证码错误').data()


"""
测试二维码
"""


@require_GET
def qrcode(request):
    # 验证验证码
    # logo = os.path.join(settings.BASE_DIR, 'public/uploads',"gl7y616B4jBA82Lq.png")
    logo = None

    img = QrcodeService().qrcode('https://music.163.com', None, logo)
    i = img
    return HttpResponse(img)


"""
测试请求
"""


@require_GET
def req(request):
    url = 'http://dmblog.youqimei.com/index/index/pages'
    # url = 'http://dmblog.youqimei.com'
    params = {'limit': 10, 'page': 1}
    headers = {"Content-Type": "application/json", "Origin": "http:XXXXX:8090", "Authorization": '1111'}
    r = RequestService().request(url, 'get', params, headers, True)
    # return HttpResponse(r)
    return ResponseService.sucess().data(r)

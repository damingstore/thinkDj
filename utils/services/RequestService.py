import requests


class RequestService():
    """
    请求服务类
    """

    # 发送请求请求
    def request(self, url, method='get', params=None, headers=None,toJson=None):
        # 整理请求参数
        args = {}
        if method == 'get' and params :
            args['params'] = params
        if method == 'post' and params:
            args['data'] = params
        if headers:
            args['headers'] = headers
        # 发送请求
        ret = requests.request(method, url, **args)
        if ret and ret.status_code == 200:
            if toJson:
                return ret.json()
            else:
                return ret
        else:
            return False

from utils.exceptions.CustomParamsException import CustomParamsException

class BasicValidate():

    def validate(self):
        if self.is_valid():
            return True
        else:
            er = self.errors
            strs = ''
            for key in er:
                strs += er[key][0] + ' '
            print(strs)
            raise CustomParamsException(strs)
from utils.exceptions.CustomException import CustomException
from utils.services.InteriorCodeService import InteriorCodeService


class CustomJwtExpiredException(CustomException):
    """
    业务异常类
    """
    code = InteriorCodeService.TOKEN_EXPIRED
    message = "签名过期"
    insideCode = InteriorCodeService.TOKEN_EXPIRED

    def __init__(self, message=None, insideCode=None, code=None):
        self.code = code and code or self.code
        self.message = message and message or self.message
        self.insideCode = insideCode and insideCode or self.insideCode
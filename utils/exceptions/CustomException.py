"""
公共异常类
"""
from utils.services.InteriorCodeService import InteriorCodeService


class CustomException(Exception):

    code = InteriorCodeService.INVALID_PARAMS
    message = "系统内部错误"
    insideCode = InteriorCodeService.INVALID_PARAMS

    def __init__(self, message=None, insideCode=None, code=None):
        self.code = code and code or self.code
        self.message = message and message or self.message
        self.insideCode = insideCode and insideCode or self.insideCode
        super().__init__()

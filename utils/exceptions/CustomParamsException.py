from utils.exceptions.CustomException import CustomException
from utils.services.InteriorCodeService import InteriorCodeService


class CustomParamsException(CustomException):
    """
    业务异常类
    """
    code = InteriorCodeService.INVALID_PARAMS
    message = "参数验证失败"
    insideCode = InteriorCodeService.INVALID_PARAMS

    def __init__(self, message=None, insideCode=None, code=None):
        self.code = code and code or self.code
        self.message = message and message or self.message
        self.insideCode = insideCode and insideCode or self.insideCode
from django.http import JsonResponse

# 权限装饰器
def authoricate(func):
    def wrap(request, *args, **kwargs):
        # 获取请求头token
        token = request.META.get('HTTP_AUTHORIZATION')
        if token:

            # 这里可以注入用户信息
            request.userInfo= 'dddd'
            return func(request, *args, **kwargs)
        else:
            # 返回错误拦截
            return JsonResponse({'code': 401, 'msg': '没有权限访问'})
    return wrap
# thinkEp架构



# 简介

thinkDj是使用python编写的后端服务架构，主要使用django框架进行封装，采用分层的思想进行封装，层次分明，架构清晰。

## **主要使用到以下技术:**

```
django(框架) 
pyjwt(token)
python-dotenv(env配置)
django-ipware(客户端IP)
django-redis(nosql)
...
```



# 项目启动

```
1：下载项目到本地
git clone https://gitee.com/damingstore/thinkDj.git

2：cd 到项目目录
cd thinkDj

3：安装依赖 
pip install

4：启动服务 
将.env.example改为.env并配置后启动服务
```



# 目录结构

```
├── thinkDj/
    ├── app(应用文件夹：可以创建多应用)
    ├──├── system(system应用) 
    ···
    ├── core(核心文件夹)
    ···
```



# 开发计划

```
2、env环境配置文件加载      -ok
4、自定义异常接管           -ok
5、控制器层                -ok
5、参数验证层              -ok
6、dao层				    -ok
7、model层				-ok
8、日志服务                -ok
9、 集成服务- jwt           -ok
10、集成服务- 文件上传       -ok
```





